<?php

/**
 * @file
 * Utility for breaking a MARC record into a usable array.
 *
 * @see http://pear.php.net/File_MARC
 */

require_once 'File/MARC.php';

/**
 * Use PEAR File_MARC to get a record from a raw MARC.
 *
 * To display record content, use object methods, such as $record->__toString().
 */
function marc_filemarc_new($marc) {
  $records = new File_MARC($marc->raw, File_MARC::SOURCE_STRING);
  $record = $records->next();
  return $record;
}
