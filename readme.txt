

                    MARC (MAchine-Readable Cataloging)
                    ==================================


-- SUMMARY --
=============
MARC (MAchine-Readable Cataloging) is the standard format for bibliographic
records for libraries. More infos about MARC can been found at http://www.loc.gov/marc.

The MARC module facilitates the importing, storage, caching and retrieval of
MARC records. The MARC Module also provides an API designed to be similar to
Drupal's node API that allows other modules to add additional functionality to
these operations.

To import MARC records administer MARC options, you need to give yourself the
proper permissions and go to Administer -> Site Building -> MARC.

Additional modules are packaged with the MARC module to take advantage and model
methods of using the MARC API.

* MARC Bib
  This module stores and retrieves bibliographic data that may be commonly
  needed. The Bib fields purposely recapitulate the data structure that is
  needed by SOPAC allowing for potential reuse of the data.

* MARC Node
  This module provides the creation of Drupal nodes upon the import of
  MARC records. The MARC Node module requires the MARC Bib module and requires
  that you map the MARC Bib data types to your Drupal node fields, CCK fields
  and taxonomy terms. This mapping is done in the Admin page at
  Administer -> Site Building -> MARC -> Node.


-- Getting Started --
=====================
To use the MARC module to import MARC records and create Drupal Nodes, follow
these steps.

* Create a content type reflecting the bibliographic data you wish to have
  available. You can use CCK fields and taxonomies.

* Install the MARC module and configure permissions.

* Go to Administer -> Site Building -> MARC -> Settings

  - Click the Settings tab and configure File Settings and Cron Option.

  - If you have the File_MARC extension enabled on your server (recommended),
    you can check the box to use that extension. Or you can use the legacy MARC
    library which is an original library of MARC tools to this module. All the
    functionality of the MARC module should work with either library but you may
    see performance gains with the File_MARC library, or you may want to use the
    functionality of the File_MARC library with your own custom module.

  - Click Save configuration to save your settings.

* Click the Node tab.

  - Choose your content type to map your MARC records to and Save Configuration.
    You can also map to multiple content types, if needed.

  - The page will reload with your mapping options.

  - For each Node field, you can choose a bib field, or you can choose not to
    map a field.

  - Save configuration.

* Click Import tab.

  - Choose file to upload.

  - Choose the content type to import to.

  - Choose to Process Records Immediately or not. If you do not process the
    records immediately, they will be imported, according to your cron options,
    incrementally when your cron is run. Processing large records sets
    immediately will take considerable time and server resources.

Your records will be imported but may or may not be searchable. This is
dependent on the Drupal search system.


-- MARC API --
==============
The MARC API lets you access and modify MARC data in your own module, via a
hook_marcapi function. For an example of usage, see both the MARC Node and MARC
bib modules.

The operations that are triggered by the API are as follows:

* build
  This is triggered, only when a MARC object is being built for caching.
  The MARC object includes not only the record but additional data added by
  other modules, such as bib data, etc.

* load
  This is triggered every time a MARC object is asked for. You can use this to
  add additional data that you may not want cached, such as shelf status.

* presave
  Data has not been saved, you can make changes to it, this is when the MARC bib
  module adds bib fields.

* save
  Data has been saved, you can now assume the data has been finalized. This is
  when the MARC node module creates the new nodes.


-- WARNING --
=============
Use at your risk. Make sure you have a backup of your files and databases so you
can roll back if necessary.


-- TROUBLESHOOTING --
=====================
See online issues at http://drupal.org/project/issues/marc.


-- LICENCE --
=============
This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


-- CONTACT --
=============
Current maintainers:
* Andrew Austin     => http://drupal.org/user/16071
* Michael Samuelson => http://drupal.org/user/47411

7.x-2.0 release:
* Daniel Berthereau (Daniel_KM) => http://drupal.org/user/428555


-- COPYRIGHT --
===============
Copyright © 2007-2010 Andrew Austin (first commit, enhancements in 6.x-2.0-beta)
Copyright © 2012 Daniel Berthereau <daniel.drupal@berthereau.net> (7.x-2.0)
