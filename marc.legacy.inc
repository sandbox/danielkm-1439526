<?php

/**
 * @file
 * This is the key utility for breaking a MARC record into a usable array.
 *
 * @note
 * This function is getting deprecated in favor of the quicker PEAR File_MARC.
 *
 */

/**
 * Just want to get marc fields out of MARC format and into a usable array.
 *
 * @see http://www.loc.gov/marc/specifications/specrecstruc.html
 */
function marc_legacy_record($marc) {
  // Create array by exploding the marc raw records.
  // Field terminator is: FT 1E(Hex), 30(Dec), 036(Oct), RS(Char).
  $marc_field_values = explode(chr(30), $marc->raw);

  // Now we need to break the leader from the directory.
  $start_length = drupal_strlen($marc_field_values[0]);
  $leader = drupal_substr($marc_field_values[0], 0, 23);
  $directory = drupal_substr($marc_field_values[0], 24, $start_length);
  $marc_field_values[0] = $leader;

  // Get the field numbers from the directory.
  // $directory_fields contaings the fieldname, the start position and the
  // length that will get taken care of in a second.
  $directory_fields = str_split($directory, 12);

  // Start building $record array.
  $record = array();

  // First we set the leader and take the leader value out of the values.
  $record['leader'] = $leader;
  array_splice($marc_field_values, 0, 1);

  // Initialize fields counts.
  $marc_field_count = array();

  // Then we loop through the directory fields and build array.
  foreach ($directory_fields as $key => $directory_field) {
    //The marc field number is just the first 3 characters
    $field_number = drupal_substr($directory_field, 0, 3);

    // We need to keep track of the field iterations.
    if (!isset($marc_field_count[$field_number])) {
      $marc_field_count[$field_number] = 0;
    }
    // Reference count to simplify.
    $field_count = &$marc_field_count[$field_number];

    // The field value is the corresponding value in the marc_field_values array.
    $field_value = $marc_field_values[$key];

    if (drupal_substr($directory_field, 0, 2) == '00') {
      // Populate control fields.
      // Technically control fields can be repeated, not that I ever see it.
      $record[$field_number][$field_count] = $field_value;

      // We need to keep track of the field iterations.
      $field_count++;
    }
    else {
      // Populate indicators.
      // NOTE To repeat indicators at data element level allow to simplify
      // management, but implies a little more memory.
      // NOTE To get exact standard MARC, use PEAR File_MARC: currently, this
      // function is only used to display the MARC tab.
      // TODO To be strictly standard compliant, need to use an array instead of
      // a string, but that means changes in other functions which use legacy
      // data.
      $i1 = drupal_substr($field_value, 0, 1);
      $i2 = drupal_substr($field_value, 1, 1);

      // Start work on subfields.
      // Subfields separator is: 1F(Hex), 31(Dec), 037(Oct), US(Char).
      $subfields = explode(chr(31), $marc_field_values[$key]);

      // Get rid of indicators.
      array_splice($subfields, 0, 1);

      // Insert subfields in record array.
      foreach ($subfields as $subfield) {
        if ($i1 !== ' ' && $i1 !== '') {
          $record[$field_number][$field_count]['i1'] = $i1;
        }
        if ($i2 !== ' ' && $i2 !== '') {
          $record[$field_number][$field_count]['i2'] = $i2;
        }

        $subfield_code = drupal_substr($subfield, 0, 1);
        $subfield_value = drupal_substr($subfield, 1);

        $record[$field_number][$field_count][$subfield_code] = $subfield_value;

        // We need to keep track of the field iterations.
        $field_count++;
      }
    }
  }

  return $record;
}
