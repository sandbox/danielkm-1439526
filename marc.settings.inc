<?php

/**
 * @file
 * Define the settings to import MARC records.
 */

/**
 * Create admin form.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function marc_admin_settings($form, &$form_state) {
  module_load_include('inc', 'marc', 'marc.field');
  global $user;

  if (module_exists('content')) {
    $message = t('To map your MARC records to more than the title and author field, visit the !link page and select the content type that you will be importing MARC records to and then select the MARC tab.', array(
      '!link' => l(t('Content types'), 'admin/structure/types'),
    ));
  }
  else {
    $message = t('By default, you can import MARC records in to the title, body fields and taxonomy fields of your content.');
  }

  $form['file'] = array(
    '#type' => 'fieldset',
    '#title' => t('File settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -6,
  );

  $form['file']['marc_file_directory_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to your MARC files'),
    '#description' => t('If blank, it will use your default files directory path.'),
    '#default_value' => variable_get('marc_file_directory_path', file_directory_path()),
  );

  $form['file']['marc_file_extensions'] = array(
    '#type' => 'textfield',
    '#title' => t('Valid MARC file extensions'),
    '#description' => t('Separate extensions with a single space.'),
    '#default_value' => variable_get('marc_file_extensions', 'mrc marc mrk 001'),
  );

  $form['cron'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cron options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -2,
  );

  $form['cron']['marc_cron_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Records per cron'),
    '#description' => t('The number of records to process during each cron.'),
    '#default_value' => variable_get('marc_cron_limit', 100),
  );

  $form['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -1,
  );

  $form['display']['marc_display_all_marc_fields'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display all imported MARC fields, even empty ones.'),
    '#default_value' => variable_get('marc_display_all_marc_fields', TRUE),
  );

  $form['libraries'] = array(
    '#type' => 'fieldset',
    '#title' => t('MARC libraries'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 0,
    '#description' => t('Choose a MARC library to use to read your records. You may choose more than one.'),
  );

  $form['libraries']['marc_library_file_marc'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use PEAR File_MARC library'),
    '#description' => t('This is a pear extension for php, which you may need to install first.'),
    '#default_value' => variable_get('marc_library_file_marc', FALSE),
  );

  $form['libraries']['marc_library_legacy'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use legacy MARC library.'),
    '#description' => t('This is the original library used by the MARC module and does not require any addition php extensions.'),
    '#default_value' => variable_get('marc_library_legacy', TRUE),
  );

  return system_settings_form($form);
}
