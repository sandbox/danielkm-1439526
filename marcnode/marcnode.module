<?php

/**
 * @file
 * MARC node API is an api to manage MARC nodes.
 *
 */

/**
 * Implements hook_menu().
 */
function marcnode_menu() {
  $items = array();

  $items['admin/config/marc/node'] = array(
    'title' => 'Node',
    'description' => 'Configure MARC Node Mapping',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('marcnode_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer marc imports'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'marcnode.settings.inc',
    'weight' => 2,
  );

  $items['node/%node/marc'] = array(
    'title' => 'MARC',
    'page callback' => 'marcnode_view_marc',
    'page arguments' => array(1),
    'access callback' => 'marcnode_node_access',
    'access arguments' => array(1),
    'weight' => 7,
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Keep MARC tab from displaying on nodes that weren't imported via MARC.
 */
function marcnode_node_access($node) {
  if (user_access('view marc record') && isset($node->nid)) {
    $access = db_query("SELECT mid FROM {marc_node} WHERE nid = :nid", array(
      ':nid' => check_plain($node->nid),
    ))->fetchField();
    if ($access) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Implements hook_node_load().
 */
function marcnode_node_load($nodes, $types) {
  // Check types to avoid hitting the database.
  $check_marcnode_types = FALSE;
  foreach ($types as $type) {
    if (variable_get('marcnode_' . $type . '_status', FALSE)) {
      $check_marcnode_types = TRUE;
      break;
    }
  }
  if (!$check_marcnode_types) {
    return NULL;
  }

  $result = db_query('SELECT nid, mid FROM {marc_node} WHERE nid IN (:nids)', array(
    ':nids' => array_keys($nodes),
  ));
  foreach ($result as $record) {
    $nodes[$record->nid]->marc = marc_load_marc($record->mid);
  }
}

/**
 * Implements hook_node_delete().
 */
function marcnode_node_delete($node) {
  if (isset($node->marc->mid) && $node->marc->mid) {
    marc_delete_marc($node->marc->mid);
  }
}

/**
 * Display a MARC node.
 */
function marcnode_view_marc($node) {
  $mid = $node->marc->mid;
  module_load_include('inc', 'marc', 'marc.pages');
  return marc_view_record($mid);
}

/**
 * Implements hook_form_alter().
 */
function marcnode_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'marc_import_form') {
    $options = array();
    $types = node_type_get_types();
    foreach ($types as $type) {
      if (variable_get('marcnode_' . $type->type . '_status', FALSE)) {
        $options[$type->type] = $type->name;
      }
    }
    if (is_array($options) && $options) {
      $form['data']['node_type'] = array(
        '#type' => 'radios',
        '#title' => t('Create content'),
        '#options' => $options,
      );
    }
    elseif (is_array($options)) {
      $form['data']['node_type_info'] = array(
        '#type'        => 'item',
        '#title' => t('Create content'),
        '#description' => t('Choose content types where to import MARC records in the "Node" tab.'),
      );
    }
    global $user;
    $form['data']['node_uid'] = array(
      '#type' => 'hidden',
      '#value' => $user->uid,
    );
  }
}

/**
 * Implements hook_marcapi_save().
 *
 * MARC has been save, mid is available.
 * You can save additional data here and now.
 */
function marcnode_marcapi_save($marc, $data = NULL) {
  $type = isset($data['node_type']) ? $data['node_type'] : '';
  if (empty($type)) {
    return;
  }

  $node = new stdClass;
  $node->type = $type;
  // Make sure we have a bundle name.
  $node->node_type = $type;
  $node->uid = isset($data['node_uid']) ? $data['node_uid'] : 0;;

  $bib_values = array();

  // Set default title even if it isn't mapped.
  $bib_values = marcnode_bib_value($marc, variable_get('marcnode_' . $type . '_title', NULL));
  if ($bib_values) {
    $node->title = current($bib_values);
  }
  else {
    $node->title = t('[No title]');
  }

  // Prepare vocabularies in case of a field is mapped with terms.
  $vocabularies = taxonomy_vocabulary_get_names();

  foreach (field_info_instances('node', $type) as $field_name => $field_instance) {
    $field = field_info_field($field_name);

    $bib_values = array();
    $field_values = array();
    foreach ($field['columns'] as $column_name => $column) {
      $map = variable_get('marcnode_' . $type . '_' . $field_name . '_' . $column_name, NULL);
      if ($map) {
        switch ($column_name) {
          case 'tid':
            $vocabulary_machine_name = $field['settings']['allowed_values'][0]['vocabulary'];
            if ($vocabulary_machine_name) {
              $terms = marcnode_bib_value($marc, $map);
              foreach ($terms as $name) {
                $term = marcnode_get_taxonomy_term($name, $vocabularies[$vocabulary_machine_name]->vid);
                $field_values[] = array(
                  'tid' => $term->tid,
                  'name' => $name,
                  'vid' => $vocabularies[$vocabulary_machine_name]->vid,
                  'vocabulary_machine_name' => $vocabulary_machine_name,
                );
              }
            }
            break;

          default:
            $bib_values[$field_name . '_' . $column_name] = marcnode_bib_value($marc, $map);

            foreach ($bib_values[$field_name . '_' . $column_name] as $key => $bib_value) {
              $field_values[$key][$column_name] = $bib_value;
            }
        }
      }
    }

    if ($field_values) {
      $node->$field_name = array('und' => $field_values);
    }
  }

  node_save($node);

  $marc_node = new stdClass;
  $marc_node->nid = $node->nid;
  $marc_node->mid = $marc->mid;
  drupal_write_record('marc_node', $marc_node);

  drupal_set_message(t('Imported <a href="@node">@title</a> from MARC file.', array(
    '@node' => url('node/' . $node->nid),
    '@title' => ($node->title),
  )));
}

/**
 * Implements hook_marcapi_delete().
 *
 * MARC record is being deleted.
 */
function marcnode_marcapi_delete($marc, $data = NULL) {
  db_delete('marc_node')
    ->condition('mid', $marc->mid)
    ->execute();
}

/**
 * Get a bibliographic field from a MARC record.
 */
function marcnode_bib_value($marc, $option) {
  if ($option) {
    $options = explode(':', $option);
    $bib_field = isset($options[0]) ? $options[0] : '';
    $marc_field = isset($options[1]) ? $options[1] : 0;
    if (isset($marc->bib->$bib_field) && is_array($marc->bib->$bib_field)) {
      if ($marc_field) {
        $field_values = ($marc->bib->$bib_field);
        $values = array($field_values['520|a']);
      }
      else {
        $values = $marc->bib->$bib_field;
      }
    }
    else {
      $values = array($marc->bib->$bib_field);
    }
    return $values;
  }

  // Else return nothing.
  return array();
}

/**
 * Get a term by name. If term doesn't exist, it's created in the specified vid.
 *
 * @return
 *   Term object.
 */
function marcnode_get_taxonomy_term($name, $vid) {
  $name = marc_clean_field($name);
  $term = db_query('SELECT * FROM {taxonomy_term_data} WHERE name = :name AND vid = :vid', array(
    ':name' => $name,
    ':vid' => $vid,
  ))->fetchObject();

  if (!$term->tid) {
    $term->vid = $vid;
    $term->name = $name;
    taxonomy_term_save($term);
  }

  return $term;
}

/**
 * Return a list of bibliographic fields.
 */
function marcnode_bib_options() {
  return array(
    'title' => t('Title'),
    'title_medium' => t('Title Medium'),
    'addl_title' => t('Additional Titles'),
    'series' => t('Series'),
    'series_num' => t('Series Number'),
    'edition' => t('Edition'),
    'author' => t('Author'),
    'addl_author' => t('Additional Authors'),
    'pub_info' => t('Publisher'),
    'pub_year' => t('Publication Year'),
    'stdnum' => t('ISBN'),
    'lccn' => t('LCCN'),
    'descr' => t('Description'),
    'notes' => t('Notes'),
    'subjects' => t('Subjects'),
    'callnum' => t('Call Number'),
    'loc_code' => t('Location Code'),
    'mat_code' => t('Material Code'),
    'notes:500|a' => t('Notes: General Note'),
    'notes:505|a' => t('Notes: Formatted Content'),
    'notes:511|a' => t('Notes: Participant or Performer'),
    'notes:520|a' => t('Notes: Summary'),
  );
}

/**
 * Get list of vocabularies for a node type.
 */
function marcnode_vocabularies($type) {
  $fields = field_info_fields();
  $existing_vocabularies = taxonomy_vocabulary_get_names();
  $vocabularies = array();
  foreach ($fields as $field_name => $field) {
    if ($field['type'] == 'taxonomy_term_reference'
        && !empty($field['bundles']['node'])
        && in_array($type, $field['bundles']['node'])
      ) {
      // Collect all vocabularies allowed for the field.
      foreach ($field['settings']['allowed_values'] as $allowed_values) {
        $vocabularies[$allowed_values['vocabulary']] = $existing_vocabularies[$allowed_values['vocabulary']]->name;
      }
    }
  }
  return $vocabularies;
}
