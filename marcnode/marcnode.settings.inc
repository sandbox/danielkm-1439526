<?php

/**
 * @file
 * Manage settings for MARC nodes.
 *
 */

/**
 * Define the settings for MARC nodes form.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function marcnode_settings($form, &$form_state) {

  $form = array();
  $NULL = array(NULL);
  $options = array(0 => t('Do not map')) + marcnode_bib_options();

  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('MARC Content Types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('These content types will be available to be created with MARC imports') . '<br />'
      . '<strong>' . t('Note') . '</strong>' . ': ' . t('You need to valid this form before to be able to map fields for selected content types.'),
  );

  $types = node_type_get_types();

  foreach ($types as $type) {
    $form['types']['marcnode_' . $type->type . '_status'] = array(
      '#type' => 'checkbox',
      '#title' => $type->name,
      '#default_value' => variable_get('marcnode_' . $type->type . '_status', FALSE),
    );
  }

  foreach ($types as $type) {
    if (variable_get('marcnode_' . $type->type . '_status', FALSE)) {
      $form[$type->type] = array(
        '#type' => 'fieldset',
        '#title' => t('Mapping of content type: !item', array('!item' => $type->name)),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      $form[$type->type]['marcnode_' . $type->type . '_title'] = array(
        '#type' => 'select',
        '#title' => t('Title'),
        '#options' => $options,
        '#default_value' => variable_get('marcnode_' . $type->type . '_title', NULL),
      );

      foreach (field_info_instances('node', $type->type) as $field_name => $field_instance) {
        $field = field_info_field($field_name);
        $columns_count = count($field['columns']);
        if (in_array('format', array_keys($field['columns']))) {
          --$columns_count;
        }

        $label = $field_instance['label'] ? $field_instance['label'] : $field_name;

        $form[$type->type]['fieldset_' . $field_name] = array(
          '#type' => 'fieldset',
          '#title' => t('Field: !label', array('!label' => $label)),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        );

        foreach ($field['columns'] as $column_name => $column) {
          // Don't map 'format' column.
          if ($column_name != 'format') {
            $label = $field_instance['label'] ? $field_instance['label'] : $field_name;
            if ($columns_count > 1) {
              $label .= ' (' . $column_name . ')';
            }

            $item = 'marcnode_' . $type->type . '_' . $field_name . '_' . $column_name;
            $form[$type->type]['fieldset_' . $field_name][$item] = array(
              '#type' => 'select',
              '#title' => $label,
              '#options' => $options,
              '#default_value' => variable_get($item, NULL),
            );
          }
        }
      }
    }
  }

  return system_settings_form($form);
}
