<?php

/**
 * @file
 * Convert a raw MARC file into an database field.
 */

/**
 * Import a raw MARC file into the database (table 'marc').
 *
 * @see http://www.loc.gov/marc/specifications/specrecstruc.html
 */
function marc_import_file($filename, $iid = NULL) {
  if (!$iid) {
    $iid = db_query('SELECT MAX(iid) FROM {marc_import}')->fetchField();
  }
  $marc_records = marc_get_contents_utf8($filename);

  // We don't use File_MARC here as it seems a bit unnecessary, we just break
  // file up into individual records.
  // Record terminator is: 1D(Hex), 29(Dec), 035(Oct), GS(Char).
  $marc_records = explode(chr(29), $marc_records);
  $count = 0;
  foreach ($marc_records as $marc_record) {
    if (drupal_strlen($marc_record) > 12) {
      $marc = new stdClass;
      $marc->iid = $iid;
      $marc->raw = $marc_record;
      $marc->created = 0;
      drupal_write_record('marc', $marc);
      $marc = NULL;
      $count++;
    }
  }

  return $count;
}

/**
 * UTF-8 fix.
 *
 * @see http://php.net/manual/en/function.file-get-contents.php
 */
function marc_get_contents_utf8($filename) {
  $content = file_get_contents($filename);
  return mb_convert_encoding($content, 'UTF-8', mb_detect_encoding($content, 'UTF-8, ISO-8859-1', TRUE));
}
