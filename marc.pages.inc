<?php

/**
 * @file
 * Dysplay MARC records in standard MARC format.
 */

/**
 * Display a themed MARC record in MARC format.
 */
function marc_view_record($mid) {
  $marc = marc_load_marc($mid);

  if (empty($marc)) {
    return t('Record "@mid" does not exist.', array('@mid' => $mid));
  }

  if (!isset($marc->legacy) || !is_array($marc->legacy)) {
    return t('You need to enable support for the Legacy MARC Library to display raw MARC records.');
  }

  $fields = $marc->legacy;

  drupal_set_title($fields['245'][0]['a']);

  $header = array(t('Field'), t('Ind.'), t('Subfield'), t('Value'));

  if (isset($fields['leader']) && $fields['leader']) {
    $rows[] = array(t('LDR'), NULL, NULL, $fields['leader']);
  }
  ksort($fields);

  $display_all = variable_get('marc_display_all_marc_fields', TRUE);

  foreach ($fields as $field => $iterations) {
    // Don't repeat leader, the only field which is not an array.
    if (is_array($iterations)) {
      foreach ($iterations as $iteration => $subfield_data) {
        if (is_array($subfield_data)) {
          $i1 = ' ';
          if (isset($subfield_data['i1'])) {
            $i1 = $subfield_data['i1'];
            unset($subfield_data['i1']);
          }
          $i2 = ' ';
          if (isset($subfield_data['i2'])) {
            $i2 = $subfield_data['i2'];
            unset($subfield_data['i2']);
          }
          if ($display_all || (current($subfield_data) !== '')) {
            $rows[] = array($field, $i1 . $i2, key($subfield_data), check_plain(current($subfield_data)));
          }
        }
        else {
          if ($display_all || ($subfield_data !== '')) {
            $rows[] = array($field, NULL, NULL, check_plain($subfield_data));
          }
        }
      }
    }
  }

  // This needs a real theme function at some point.
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No MARC record found.')));

  return $output;
}
